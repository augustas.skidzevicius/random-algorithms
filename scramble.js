const scramble = function (string1, string2) {
  if (string1.length < string2.length) return false;
  else if (string1 === string2) return true;

  let newString = '';
  for (let i = 0; i < string2.length; i++) {
    if (i === 0) {
      if (string1.indexOf(string2[i]) === -1) return false;
      newString = string1.split('').filter(char => char !== string1[string1.indexOf(string2[i])]).join('');
    } else {
      if (newString.indexOf(string2[i]) === -1) return false;
      newString = newString.split('').filter(char => char !== newString[newString.indexOf(string2[i])]).join('');
    }
  }

  return true;
}

module.exports = scramble;
