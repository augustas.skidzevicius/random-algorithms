// scramble('rkqodlw', 'world') ==> True
// scramble('rkqodlw', 'worldd') ==> False
// scramble('amasis', 'visma') ==> False
const scramble = require('./scramble');

test('lalalalala', () => {
  expect(scramble('rkqodlw', 'world')).toBe(true);
});
